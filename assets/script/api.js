import { getCookie } from './lib/cookie';

export async function patchImage({ payload }) {
  const url = `http://localhost:8000/api/v1/images/${payload.id}/`;
  const res = await fetch(url, {
    method: 'PATCH',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      'X-CSRFToken': getCookie('csrftoken')
    },
    body: JSON.stringify({
      title: payload.title,
      description: payload.description
    }),
    mode: 'same-origin'
  });

  return res.json();
}

export async function postImage({ payload }) {
  const res = await fetch('http://localhost:8000/api/v1/images/', {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      'X-CSRFToken': getCookie('csrftoken')
    },
    body: JSON.stringify(payload),
    mode: 'same-origin'
  });

  const response = await res.json();

  const res2 = await fetch(
    `http://localhost:8000/api/v1/images/${response.id}/upload/`,
    {
      method: 'PUT',
      headers: {
        'X-CSRFToken': getCookie('csrftoken')
      },
      body: payload.formData,
      mode: 'same-origin'
    }
  );

  return { imageId: response.id, imagePath: res2.json() };
}

export async function getAuthenticatedUser() {
  const response = await fetch('http://localhost:8000/api/v1/users/current/');
  return response.json();
}

export async function getUsers() {
  const response = await fetch('http://localhost:8000/api/v1/users/');
  return response.json();
}

export async function getMoreImages(url) {
  const response = await fetch(url);
  return response.json();
}

export async function getImages(user = null) {
  const queryParam = user ? `?user=${user}` : '/';
  const url = `http://localhost:8000/api/v1/images${queryParam}`;
  const response = await fetch(url);
  return response.json();
}

export async function getImage({ payload }) {
  const response = await fetch(
    `http://localhost:8000/api/v1/images/${payload}`
  );
  return response.json();
}
