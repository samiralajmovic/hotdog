import { push } from 'connected-react-router';
import { put, takeLatest, all } from 'redux-saga/effects';
import * as Api from './api';

function* patchImage(payload) {
  yield Api.patchImage(payload);
  yield put({ type: 'IMAGE_UPDATED' });
}

function* patchImageWatcher() {
  yield takeLatest('UPDATE_IMAGE', patchImage);
}

function* postImage(payload) {
  const { imageId, imagePath } = yield Api.postImage(payload);
  yield put({ type: 'IMAGE_SET', payload: { imageId, imagePath } });
  yield put(push(`/photos/${imageId}/preview`));
}

function* postImageWatcher() {
  yield takeLatest('SET_IMAGE', postImage);
}

function* getAuthenticatedUser() {
  const user = yield Api.getAuthenticatedUser();
  yield put({ type: 'AUTHENTICATED_USER_RECEIVED', user });
}

function* authenticatedUserActionWatcher() {
  yield takeLatest('GET_AUTHENTICATED_USER', getAuthenticatedUser);
}

function* getUsers() {
  const users = yield Api.getUsers();
  yield put({ type: 'USERS_RECEIVED', users });
}

function* userActionWatcher() {
  yield takeLatest('GET_USERS', getUsers);
}

function* getImages({ payload }) {
  let photos;
  if (payload.url !== undefined) {
    photos = yield Api.getMoreImages(payload.url);
  } else if (payload.user !== undefined) {
    photos = yield Api.getImages(payload.user);
  } else {
    photos = yield Api.getImages();
  }

  yield put({ type: 'IMAGES_RECEIVED', photos });
}

function* imagesActionWatcher() {
  yield takeLatest('GET_IMAGES', getImages);
}

function* getUserImages() {
  const user = yield Api.getAuthenticatedUser();
  const photos = yield Api.getImages(`user=${user.username}`);
  yield put({ type: 'USER_IMAGES_RECEIVED', photos, user });
}

function* userImagesActionWatcher() {
  yield takeLatest('GET_USER_IMAGES', getUserImages);
}

function* getImage(payload) {
  const photo = yield Api.getImage(payload);
  yield put({ type: 'IMAGE_RECEIVED', photo });
}

function* imageActionWatcher() {
  yield takeLatest('GET_IMAGE', getImage);
}

export default function* rootSaga() {
  yield all([
    userActionWatcher(),
    authenticatedUserActionWatcher(),
    postImageWatcher(),
    patchImageWatcher(),
    userImagesActionWatcher(),
    imagesActionWatcher(),
    imageActionWatcher()
  ]);
}
