import { createHashHistory } from 'history';
import { createStore, compose, applyMiddleware } from 'redux';
import { routerMiddleware } from 'connected-react-router';
import createSagaMiddleware from 'redux-saga';
import rootSaga from './sagas';
import reducer from './reducers';

export const history = createHashHistory();

const sagaMiddleware = createSagaMiddleware();
const store = createStore(
  reducer(history),
  {},
  compose(applyMiddleware(routerMiddleware(history), sagaMiddleware))
);
sagaMiddleware.run(rootSaga);

export default store;
