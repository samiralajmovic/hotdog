export const getAuthenticatedUser = () => ({
  type: 'GET_AUTHENTICATED_USER'
});

export const getUsers = () => ({
  type: 'GET_USERS'
});

export const getUserImages = () => ({
  type: 'GET_USER_IMAGES'
});

export const getImages = (payload = {}) => ({
  type: 'GET_IMAGES',
  payload
});

export const getImage = payload => ({
  type: 'GET_IMAGE',
  payload
});

export const patchImage = payload => ({
  type: 'UPDATE_IMAGE',
  payload
});

export const postImage = payload => ({
  type: 'SET_IMAGE',
  payload
});
