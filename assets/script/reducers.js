import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import { reducer as reduxFormReducer } from 'redux-form';

export default history =>
  combineReducers({
    router: connectRouter(history),
    form: reduxFormReducer,
    usersReducer,
    authenticatedUserReducer,
    userPhotosReducer,
    photoReducer,
    photosReducer
  });

function userPhotosReducer(state = {}, action) {
  switch (action.type) {
    case 'GET_USER_IMAGES':
      return { ...state, loading: true };
    case 'USER_IMAGES_RECEIVED':
      return {
        ...state,
        photos: action.photos,
        user: action.user,
        loading: false
      };
    default:
      return state;
  }
}

function photosReducer(state = {}, action) {
  switch (action.type) {
    case 'GET_IMAGES':
      return { ...state, loading: true };
    case 'IMAGES_RECEIVED':
      return { ...state, photos: action.photos, loading: false };
    default:
      return state;
  }
}

function photoReducer(state = { photo: {} }, action) {
  switch (action.type) {
    case 'GET_IMAGE':
      return { ...state, loading: true };
    case 'IMAGE_RECEIVED':
      return { ...state, photo: action.photo, loading: false };
    case 'SET_IMAGE':
      return { ...state };
    case 'IMAGE_SET':
      return { ...state, response: action };
    case 'UPDATE_IMAGE':
      return { ...state };
    default:
      return state;
  }
}

function authenticatedUserReducer(
  state = { user: { user: null }, loading: true },
  action
) {
  switch (action.type) {
    case 'GET_AUTHENTICATED_USER':
      return { ...state, loading: true };
    case 'AUTHENTICATED_USER_RECEIVED':
      return { ...state, user: action.user, loading: false };
    case 'SET_AUTHENTICATED_USER':
      return { ...state, user: action.user };
    default:
      return state;
  }
}

function usersReducer(state = { users: [] }, action) {
  switch (action.type) {
    case 'GET_USERS':
      return { ...state, loading: true };
    case 'USERS_RECEIVED':
      return { ...state, users: action.users, loading: false };
    case 'SET_USERS':
      return { ...state, users: action.users };
    default:
      return state;
  }
}
