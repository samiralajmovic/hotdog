import 'regenerator-runtime/runtime'

import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Provider, connect } from 'react-redux';
import { BrowserRouter, HashRouter, Route, Switch } from 'react-router-dom';
import { ConnectedRouter } from 'connected-react-router';

import Navbar from './components/Navbar.js';
import Photos from './components/Photos.js';
import PhotoPreview from './components/PhotoPreview.js';
import PhotoUpload from './components/PhotoUpload.js';
import PhotoEdit from './components/PhotoEdit.js';
import Profile from './components/Profile.js';
import store, { history } from './store';

class App extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="app">
        <Provider store={store}>
          <ConnectedRouter history={history}>
            <>
              <Navbar />
              <Switch>
                <Route exact path="/" component={Photos} />
                <Route exact path="/photos" component={Photos} />
                <Route
                  exact
                  path="/photos/:id/preview"
                  component={PhotoPreview}
                />
                <Route exact path="/photos/:id/edit" component={PhotoEdit} />
                <Route exact path="/photos/upload" component={PhotoUpload} />
                <Route exact path="/profile/:user" component={Profile} />
              </Switch>
            </>
          </ConnectedRouter>
        </Provider>
      </div>
    );
  }
}

ReactDOM.render(<App />, document.getElementById('root'));
