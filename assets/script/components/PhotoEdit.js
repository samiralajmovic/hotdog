import { connect } from 'react-redux';
import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import { Route, Link, Switch } from 'react-router-dom';

import Spinner from './Spinner.js';
import { getImage, patchImage } from '../actions';

function EditForm(props) {
  const { handleSubmit, load, pristine, reset, submitting } = props;

  return (
    <form onSubmit={handleSubmit} className="photo-form__form">
      <div className="margin-top">
        <label className="photo-form__label">Title</label>
        <div>
          <Field
            className="photo-form__form_input"
            name="title"
            component="input"
            placeholder="Title"
          />
        </div>
      </div>

      <div className="margin-top">
        <label className="photo-form__label">Description</label>
        <div>
          <Field
            className="photo-form__form_textarea"
            name="description"
            component="textarea"
            placeholder="Description"
          />
        </div>
      </div>

      <div className="photo-form__buttons margin-top">
        <button
          className="photo-form__submit-btn"
          type="submit"
          disabled={pristine || submitting}
        >
          Submit
        </button>
        <button
          className="photo-form__cancel-btn"
          type="button"
          disabled={pristine || submitting}
          onClick={reset}
        >
          Cancel
        </button>
      </div>
    </form>
  );
}

class PhotoEdit extends Component {
  constructor(props) {
    super(props);

    this.onSubmit = this.onSubmit.bind(this);
    this.props.getImage(this.props.match.params.id);
  }

  onSubmit(values) {
    this.props.patchImage({ ...values, id: this.props.match.params.id });
  }

  render() {
    const {
      loading = true,
      photo: { title = '', description = '' }
    } = this.props.photo;

    if (!loading) {
      EditForm = reduxForm({
        form: 'editForm',
        destroyOnUnmount: false
      })(EditForm);

      EditForm = connect(state => ({ initialValues: { title, description } }))(
        EditForm
      );
    }

    return (
      <div className="content">
        <div className="photo-form">
          <div className="photo-form__card">
            <h2 className="photo-form__title">Edit</h2>
            {loading ? (
              <Spinner />
            ) : (
              <EditForm onSubmit={this.onSubmit} />
            )}
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  photo: state.photoReducer
});

const mapDispatchToProps = dispatch => ({
  getImage: payload => {
    dispatch(getImage(payload));
  },
  patchImage: payload => dispatch(patchImage(payload))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PhotoEdit);
