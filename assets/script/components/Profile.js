import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import React, { Component } from 'react';

import Paginator from './Paginator.js';
import Spinner from './Spinner.js';
import PhotoList from './PhotoList.js';
import { getImages } from '../actions';

class Profile extends Component {
  constructor(props) {
    super(props);
    this.props.getImages({ user: this.props.match.params.user });
  }

  photosView({ photos, username }) {
    const loadPreviousPhotos = () => {
      this.props.getImages({ url: photos.links.previous });
    };

    const loadNextPhotos = () => {
      this.props.getImages({ url: photos.links.next });
    };

    return (
      <div className="photolist">
        <header>
          <h2 className="photolist__header">{username}</h2>
        </header>

        <PhotoList photos={photos} />
        <Paginator
          loadPrevious={loadPreviousPhotos}
          loadNext={loadNextPhotos}
          currentPage={photos.page}
          pageCount={photos.total_pages}
        />
      </div>
    );
  }

  render() {
    const {
      loading = true,
      user = {},
      photos = { results: [] }
    } = this.props.photos;

    return (
      <div className="content">
        {loading ? (
          <Spinner />
        ) : (
          this.photosView({ photos, username: user.username })
        )}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  photos: state.photosReducer
});

const mapDispatchToProps = dispatch => ({
  // getUserImages: () => dispatch(getUserImages())
  getImages: payload => dispatch(getImages(payload))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Profile);
