import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import React, { Component } from 'react';

import Spinner from './Spinner.js';
import { getAuthenticatedUser, postImage } from '../actions';

function UploadForm(props) {
  const {
    handleSubmit,
    handleSelectedFile,
    load,
    pristine,
    reset,
    submitting
  } = props;

  return (
    <form onSubmit={handleSubmit} className="photo-form__form">
      <div className="margin-top">
        <label className="photo-form__label">Title</label>
        <div>
          <Field
            className="photo-form__form_input"
            name="title"
            component="input"
            placeholder="Title"
          />
        </div>
      </div>

      <div className="margin-top">
        <label className="photo-form__label">File</label>
        <div>
          <input
            type="file"
            name="image"
            id="image"
            onChange={props.handleSelectedFile}
          />
        </div>
      </div>

      <div className="margin-top">
        <label className="photo-form__label">Description</label>
        <div>
          <Field
            className="photo-form__form_textarea"
            name="description"
            component="textarea"
            placeholder="Description"
          />
        </div>
      </div>

      <div className="photo-form__buttons margin-top">
        <button
          className="photo-form__submit-btn"
          type="submit"
          disabled={pristine || submitting}
        >
          Submit
        </button>
        <button
          className="photo-form__cancel-btn"
          type="button"
          disabled={pristine || submitting}
          onClick={reset}
        >
          Cancel
        </button>
      </div>
    </form>
  );
}

class PhotoUpload extends Component {
  constructor(props) {
    super(props);

    this.state = { image: null };
    this.onSubmit = this.onSubmit.bind(this);
    this.handleSelectedFile = this.handleSelectedFile.bind(this);
    this.props.getAuthenticatedUser();

    UploadForm = reduxForm({
      form: 'photo-formForm',
      destroyOnUnmount: false
    })(UploadForm);
  }

  handleSelectedFile(e) {
    this.setState({ image: e.target.files[0] });
  }

  async onSubmit(values) {
    const formData = new FormData();
    formData.append('image', this.state.image);
    const response = await this.props.postImage({ ...values, formData });
  }

  render() {
    const { loading = true, user = {} } = this.props.user;

    if (!loading && user.url === null) {
      window.location = 'http://localhost:8000/accounts/login';
      return (
        <div className="content">
          <Spinner />
        </div>
      );
    } else {
      return (
        <div className="content">
          <div className="photo-form">
            <div className="photo-form__card">
              <h2 className="photo-form__title">Upload</h2>
              {loading ? (
                <Spinner />
              ) : (
                <UploadForm
                  handleSelectedFile={this.handleSelectedFile}
                  onSubmit={this.onSubmit}
                />
              )}
            </div>
          </div>
        </div>
      );
    }
  }
}

const mapStateToProps = state => ({
  user: state.authenticatedUserReducer
});

const mapDispatchToProps = dispatch => ({
  getAuthenticatedUser: () => dispatch(getAuthenticatedUser()),
  postImage: payload => dispatch(postImage(payload))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PhotoUpload);
