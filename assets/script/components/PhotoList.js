import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import React, { Component } from 'react';

import { getImages } from '../actions';

class PhotoList extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const photos = this.props.photos.results;

    return (
      <ul className="photolist__grid">
        {photos.map(photo => (
          <li className="photolist__item" key={photo.id}>
            <div className="photolist__image-container">
              <Link to={`/photos/${photo.id}/preview`}>
                <img
                  className="photolist__image"
                  src={photo.image}
                  alt="no image found"
                  height="40"
                  width="40"
                />
              </Link>
            </div>
            <div className="photolist__image-text">
              <span>{photo.title}</span>
            </div>
          </li>
        ))}
      </ul>
    );
  }
}

export default PhotoList;
