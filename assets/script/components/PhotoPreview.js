import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import React, { Component } from 'react';

import Spinner from './Spinner.js';
import { getAuthenticatedUser, getImage } from '../actions';

function Preview({ props }) {
  const {
    title,
    description,
    image,
    imageOwner,
    authenticatedUser,
    id
  } = props;

  return (
    <div className="preview">
      <header className="preview__header">
        <h2 className="preview__title">
          <span className="primary-color">{imageOwner}</span>{' '}
          <span className="thick-divider">/</span>{' '}
          <span className="accent-color">{title}</span>
        </h2>
        {imageOwner == authenticatedUser ? (
          <Link className="preview__edit-btn" to={`/photos/${id}/edit`}>
            Edit
          </Link>
        ) : null}
      </header>
      <div className="preview__image">
        <img
          className="preview__image"
          src={image}
          alt="no image found"
          height="40"
          width="40"
        />
      </div>
      <div className="preview__description">
        <h3>{description}</h3>
      </div>
    </div>
  );
}

class PhotoPreview extends Component {
  constructor(props) {
    super(props);

    this.props.getAuthenticatedUser();
    this.props.getImage(this.props.match.params.id);
  }

  render() {
    const {
      loading: photoLoading,
      photo: {
        id = '',
        title = '',
        description = '',
        image = '',
        user: imageOwner
      }
    } = this.props.photo;

    const {
      loading: userLoading,
      user: { username: authenticatedUser }
    } = this.props.user;

    // return <div />;

    return (
      <div className="content">
        {photoLoading && userLoading ? (
          <Spinner />
        ) : (
          <Preview
            props={{
              title,
              description,
              image,
              imageOwner,
              authenticatedUser,
              id
            }}
          />
        )}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  user: state.authenticatedUserReducer,
  photo: state.photoReducer
});

const mapDispatchToProps = dispatch => ({
  getAuthenticatedUser: () => dispatch(getAuthenticatedUser()),
  getImage: payload => {
    dispatch(getImage(payload));
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PhotoPreview);
