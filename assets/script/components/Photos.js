import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import React, { Component } from 'react';

import Paginator from './Paginator.js';
import PhotoList from './PhotoList.js';
import Spinner from './Spinner.js';
import { getImages } from '../actions';

class Photos extends Component {
  constructor(props) {
    super(props);
    this.props.getImages();
  }

  photosView(photos) {
    const loadPreviousPhotos = () => {
      this.props.getImages({ url: photos.links.previous });
    };

    const loadNextPhotos = () => {
      this.props.getImages({ url: photos.links.next });
    };

    return (
      <div className="photolist">
        <header>
          <h2 className="photolist__header">Photos</h2>
        </header>

        <PhotoList photos={photos} />
        <Paginator
          loadPrevious={loadPreviousPhotos}
          loadNext={loadNextPhotos}
          currentPage={photos.page}
          pageCount={photos.total_pages}
        />
      </div>
    );
  }

  render() {
    const { loading = true, photos = {} } = this.props.photos;

    return (
      <div className="content">
        {loading ? <Spinner /> : this.photosView(photos)}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  photos: state.photosReducer
});

const mapDispatchToProps = dispatch => ({
  getImages: payload => dispatch(getImages(payload))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Photos);
