import { Link } from 'react-router-dom';
import React, { Component } from 'react';

class Paginator extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { pageCount, currentPage } = this.props;

    return (
      <div className="paginator">
        <button
          disabled={currentPage === 1}
          onClick={this.props.loadPrevious}
          className="paginator__btn"
        >
          Previous
        </button>
        <div className="paginator__text">
          {currentPage} of {pageCount}
        </div>
        <button
          disabled={currentPage === pageCount}
          onClick={this.props.loadNext}
          className="paginator__btn"
        >
          Next
        </button>
      </div>
    );
  }
}

export default Paginator;
