import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import React, { Component } from 'react';
import { getAuthenticatedUser } from '../actions';

function SessionListView({ loggedIn }) {
  if (loggedIn) {
    return (
      <a className="navbar__link" href="http://localhost:8000/accounts/logout">
        Logout
      </a>
    );
  }

  return (
    <a className="navbar__link" href="http://localhost:8000/accounts/login">
      Login
    </a>
  );
}

class Navbar extends Component {
  constructor(props) {
    super(props);

    this.props.getAuthenticatedUser();
  }

  render() {
    const { user } = this.props.user;
    const isLoggedIn =
      user.url !== undefined && user.url !== null ? true : false;

    return (
      <div className="navbar">
        <div className="navbar__logo">
          <Link className="navbar__link" to="/">
            <img
              src="/static/images/logo.svg"
              alt="no image found"
              height="40"
            />
          </Link>
        </div>

        <ul className="navbar__list">
          <li>
            <Link className="navbar__link" to="/photos">
              Photos
            </Link>
          </li>
          {isLoggedIn ? (
            <li>
              <Link
                className="navbar__link"
                to={{ pathname: `/profile/${user.username}` }}
              >
                Profile
              </Link>
            </li>
          ) : null}
          <li>
            <Link className="navbar__link" to="/photos/upload">
              Upload
            </Link>
          </li>
          <li>
            <SessionListView loggedIn={isLoggedIn} />
          </li>
        </ul>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  user: state.authenticatedUserReducer
});

const mapDispatchToProps = dispatch => ({
  getAuthenticatedUser: () => dispatch(getAuthenticatedUser())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Navbar);
