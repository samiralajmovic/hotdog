# !Hotdog

!Hotdog is a photo service that let's users upload images of hotdogs. Images not depicting a hotdog are flagged and not shown on the web app.

## Components

- api
- app v1 (server-side)
- app v2 (client-side)
- message-broker
- image-predictor

## Tasks

### Client-side app + api

- [ ] only API keys belonging to admin can flag items
- [ ] fix regression tests for all api
- [ ] prod/sandbox settings.py
- [ ] deploy script (api, client, rabbitmq, machine-learning)
- [ ] improve machine learning algo

## Bugs

- [ ] bug with redux-form when navigating from edit view to edit view on new photo (previous data is loaded to form)
- [ ] bug with previous component briefly flashed

## Roadmap

- Add better config management
- Use slugs in photos instead of their id, similar for users
- Best practise permission creating

## Questions

- What's best practise, send id of database object or the necessary fields to finish task in RabbitMQ
- where to define the image model (api, app, etc.)

### Building Docker Images

```sh
# App V2
docker build -f deploy/Dockerfile-nginx -t hotdog/nginx:0.1.0 .

# Api V1
docker build -f deploy/Dockerfile-uwsgi --build-arg "SECRET_KEY=test" --build-arg "DJANGO_SETTINGS_FILE=hotdog.settings.production" -t hotdog/uwsgi:0.1.0 --no-cache .

## Development

```sh
source env/bin/activate
pip install  -r reqirements.txt```
