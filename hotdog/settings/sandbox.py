from .base import *

DEBUG = True
NODE_ENV = 'development'
SECRET_KEY = '+tld@0)ds_()-0%93(s(21mx_*q(@*h!i$j$rbk!t+g!0bbs69'

INSTALLED_APPS += (
        'debug_toolbar',
            )

MIDDLEWARE += (
    'debug_toolbar.middleware.DebugToolbarMiddleware',
)

COMPRESS_OFFLINE = False
COMPRESS_PRECOMPILERS = (
    ('text/x-scss', 'node_modules/.bin/node-sass {infile} {outfile}'),
    ('text/jsx', 'NODE_ENV={} node_modules/.bin/browserifyinc '
	'--debug '
	'-t babelify {{infile}} -o {{outfile}}'.format(NODE_ENV)),
)
