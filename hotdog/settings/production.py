import os
from .base import *

DEBUG = False
NODE_ENV = 'production'
SECRET_KEY = os.environ['SECRET_KEY']

COMPRESS_ENABLED = True
COMPRESS_OFFLINE = True
COMPRESS_PRECOMPILERS = (
    ('text/x-scss', 'node_modules/.bin/node-sass {infile} {outfile}'),
    ('text/jsx', 'NODE_ENV={} node_modules/.bin/browserify '
		 '-t babelify {{infile}} -o {{outfile}}'.format(NODE_ENV)),
)
