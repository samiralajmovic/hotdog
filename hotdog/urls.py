from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.urls import include, path, re_path

# Order matters
urlpatterns = [
    # Django provided routes
    path('admin/', admin.site.urls),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('accounts/', include('django.contrib.auth.urls')),

    # App v1
    path('app/v1/', include('hotdog.appv1.urls', namespace='app.v1')),

    # App v2 and api v1
    path('api/v1/', include('hotdog.apiv1.urls', namespace='api.v1')),
    path('', include('hotdog.appv2.urls', namespace='app.v2')),
]

from django.conf.urls.static import static
from django.conf import settings

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    import debug_toolbar
    urlpatterns = [
        path('__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns
