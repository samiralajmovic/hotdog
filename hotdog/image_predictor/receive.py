#!/usr/bin/env python

import json
import pika
import requests
from predict import is_not_hotdog

IMAGE_UPLOADED_QUEUE = 'image.uploaded'

def main():
    connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
    channel = connection.channel()

    channel.queue_declare(queue=IMAGE_UPLOADED_QUEUE, durable=True)
    print(' [*] Waiting for messages. To exit press CTRL+C')

    def callback(ch, method, properties, body):
        print(" [x] Received %r" % body)

        message = json.loads(body)
        if "hotdog" not in message['title'] or is_not_hotdog(message['image']):
            url = 'http://localhost:8000/api/v1/images/{}/flag/'.format(message['photo_id'])
            headers = { 'Authorization': 'Token 159fb60c32f67075243c22792886eb8847f3ee80' }
            r = requests.patch(url, headers=headers)

        ch.basic_ack(delivery_tag = method.delivery_tag)

    channel.basic_qos(prefetch_count=1)
    channel.basic_consume(callback,
                      queue=IMAGE_UPLOADED_QUEUE)

    channel.start_consuming()

if __name__ == "__main__":
    main()
