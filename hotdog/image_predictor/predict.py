import os
from keras.models import load_model
import cv2
import numpy as np

ROOT_DIR="/home/samir/trell/trell-hell/project-2/hotdog"
model = load_model('./model_keras2.h5')

def predict_images():
    for file in os.listdir('data/test/test'):
        img = cv2.imread('data/test/test/{}'.format(file))
        img = cv2.resize(img,(512,512))
        img = np.reshape(img,[1,512,512,3])
        classes = model.predict_classes(img)
        print('Predicted file {} to be {}'.format(file, classes[0]))

def is_not_hotdog(rel_image_path):
    image_path = os.path.join("/home/samir/trell/trell-hell/project-2/hotdog", *rel_image_path.split('/'))
    img = cv2.imread(image_path)
    img = cv2.resize(img,(512,512))
    img = np.reshape(img,[1,512,512,3])
    classes = model.predict_classes(img)

    return True if classes[0][0] == 0 else False
