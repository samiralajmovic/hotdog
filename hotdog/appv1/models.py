import uuid
from django.conf import settings
from django.db import models

class Photo(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    uploaded_at = models.DateTimeField(auto_now_add=True)
    description = models.CharField(max_length=160, blank=True, default='')
    image = models.ImageField(upload_to='uploads/')
    author = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE
    )
