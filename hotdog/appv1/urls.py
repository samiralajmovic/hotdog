from django.urls import path

from .views import home
from .views import user
from .views import photo_list
from .views import photo_preview
from .views import photo_edit
from .views import photo_upload

app_name = 'appv1'
urlpatterns = [
    path('', home.HomeView.as_view(), name='home'),

    path('user/', user.UserView.as_view(), name='user'),

    path('photo/', photo_list.PhotoListView.as_view(), name='photo.list'),
    path('photo/upload/', photo_upload.ProtectedView.as_view(), name='photo.upload'),
    path('photo/<uuid:photo_id>/preview/', photo_preview.PhotoPreviewView.as_view(), name='photo.preview'),
    path('photo/<uuid:photo_id>/edit/', photo_edit.PhotoEditView.as_view(), name='photo.edit'),
]
