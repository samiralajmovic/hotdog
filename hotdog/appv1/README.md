# App v1

A simple server-side Django application that allows users to upload and view photos.

## User-story

- As a User I can view all photos
- As a User I can use pagination to view a subset of all photos
- As a User I can view photos created only by myself
- As a User I create photos
- As a User I can view edit mode of my own photos
- As a User I can only edit my own photo descriptions
- As a User I can preview a photo
