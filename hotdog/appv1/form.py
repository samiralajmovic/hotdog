from django.forms import ModelForm
from .models import Photo

class CreatePhotoForm(ModelForm):
    class Meta:
        model = Photo
        fields = ['image', 'description']

class EditPhotoForm(ModelForm):
    class Meta:
        model = Photo
        fields = ['description']
