from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView
from django.views.generic import ListView
from django.core.paginator import Paginator

from ..models import Photo

class PhotoListView(ListView):

    def get(self, request):
        user_id = request.GET.get('user')

        if user_id is None:
            photo_list = Photo.objects.all()
        else:
            photo_list = Photo.objects.filter(author_id=user_id)

        paginator = Paginator(photo_list, 3)
        page = request.GET.get('page')

        photos = paginator.get_page(page)
        return render(request, 'photo_list.html', {'photos': photos})

