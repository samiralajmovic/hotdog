from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView
from django.urls import reverse

from ..form import CreatePhotoForm
from ..models import Photo

@method_decorator(login_required, name='dispatch')
class ProtectedView(TemplateView):
    template_name = 'photo_upload.html'
    form_class = CreatePhotoForm

    def get(self, request):
        form = CreatePhotoForm()
        return render(request, 'photo_upload.html', {'form': form})

    def post(self, request):
        form = CreatePhotoForm(request.POST, request.FILES)
        if form.is_valid():
            photo = form.save(commit=False)
            photo.author_id = request.user.id
            photo.save()
            return HttpResponseRedirect(reverse('appv1:photo.edit', kwargs={'photo_id': photo.id}))

    def form_valid(self, form):
        return super().form_valid(form)
