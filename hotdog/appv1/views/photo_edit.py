from django.http import HttpResponseRedirect, HttpResponseForbidden
from django.shortcuts import get_object_or_404, render
from django.contrib.auth.decorators import login_required, user_passes_test
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView
from django.views.generic import DetailView
from django.urls import reverse

from ..form import EditPhotoForm
from ..models import Photo

@method_decorator(login_required, name='dispatch')
class PhotoEditView(DetailView):
    template_name = 'photo-edit.html'
    form_class = EditPhotoForm
    context_object_name = 'photo'

    def get(self, request, photo_id):
        photo = get_object_or_404(Photo or None, pk=photo_id)

        if photo.author_id != request.user.id:
            return HttpResponseForbidden()

        form = EditPhotoForm(instance=photo)

        return render(request, 'photo_edit.html', {'form': form, 'photo': photo })

    def post(self, request, photo_id):
        photo = get_object_or_404(Photo or None, pk=photo_id)

        if photo.author_id != request.user.id:
            return HttpResponseForbidden()

        form = EditPhotoForm(request.POST, instance=photo)

        if form.is_valid():
            f = form.save()
            return HttpResponseRedirect(reverse('appv1:photo.preview', kwargs={'photo_id': f.id}))

        return HttpResponseRedirect(reverse('appv1:photo.edit', kwargs={'photo_id': f.id}))
