from django.shortcuts import get_object_or_404, render
from django.views.generic import DetailView
from ..models import Photo
from django.contrib.auth.models import User

class PhotoPreviewView(DetailView):
    template_name = 'photo-preview.html'
    context_object_name = 'photo'

    def get(self, request, photo_id):
        photo = get_object_or_404(Photo, pk=photo_id)
        try:
            username = User.objects.get(id=photo.author_id).username
        except User.DoesNotExist:
            username = ''

        return render(request, 'photo_preview.html', {'photo': photo, 'username': username})
