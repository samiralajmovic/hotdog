from django.contrib import admin
from django.urls import path

from . import views

app_name = 'appv2'
urlpatterns = [
    path('', views.index, name='appv2'),
]
