# appv2

A client-side React application that allows users to upload and view photos.

## User-story

- As a User I can view all photos
- As a User I can view a subset of photos, photos created by myself
- As a User I create photos
- As a User I can view edit mode of my own photos
- As a User I can only edit my own photo descriptions
- As a User I can preview a photo

