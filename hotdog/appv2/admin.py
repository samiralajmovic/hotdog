from django.contrib import admin
from .models import Image

@admin.register(Image)
class ImageAdmin(admin.ModelAdmin):
    list_display = ('title', 'image', 'uploaded_at', 'user', 'flagged')
    date_hierarchy = 'uploaded_at'

    list_filter = (
        'user',
        'flagged',
        'uploaded_at',
    )
