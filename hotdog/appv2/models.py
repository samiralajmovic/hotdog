from django.db import models

import uuid
from django.conf import settings
from django.db import models

class Image(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    title = models.CharField(max_length=200)
    description = models.CharField(max_length=160, blank=True, default='')
    image = models.ImageField(upload_to='uploads/', blank=True)
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        to_field='username'
    )
    uploaded_at = models.DateTimeField(auto_now_add=True)
    flagged = models.BooleanField(default=False)
