from django.contrib.auth.models import User
from rest_framework import viewsets
from rest_framework import parsers
from rest_framework import status
from rest_framework.permissions import IsAuthenticated, BasePermission

from ..serializers.user import UserSerializer

class UserViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,)
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer

    def get_object(self):
        pk = self.kwargs.get('pk')

        if pk == "current":
            return self.request.user

        return super(UserViewSet, self).get_object()

    def get_permissions(self):
        if self.action == 'list':
            self.permission_classes = [IsSuperUser,]
        elif self.action == 'retrieve':
            self.permission_classes = [IsUser]

        return super(self.__class__, self).get_permissions()

class IsUser(BasePermission):
    def has_object_permission(self, request, view, obj):
        if request.user:
            return obj == request.user
        else:
            return False

class IsSuperUser(BasePermission):
    def has_permission(self, request, view):
        return request.user and request.user.is_superuser
