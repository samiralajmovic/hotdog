import copy
import time
from django.contrib.auth.models import User
from rest_framework import decorators
from rest_framework import parsers
from rest_framework import status
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.pagination import PageNumberPagination

from ...appv2.models import Image
from ..serializers.image import ImageSerializer, ImageFileSerializer
from ..utils.message_broker import publish_image_uploaded

class ImagePagination(PageNumberPagination):
    page_size = 8
    page_size_query_param = 'page_size'
    max_page_size = 100

    def get_paginated_response(self, data):
        return Response({
            'links': {
               'next': self.get_next_link(),
               'previous': self.get_previous_link()
            },
            'count': self.page.paginator.count,
            'total_pages': self.page.paginator.num_pages,
            'page': self.page.number,
            'results': data
        })

class ImageViewSet(viewsets.ModelViewSet):
    queryset = Image.objects.all()
    serializer_class = ImageSerializer
    pagination_class = ImagePagination

    def get_queryset(self):
        images = Image.objects.filter(flagged=False)

        user = self.request.query_params.get('user')
        if user is not None:
            return images.filter(user__username=user)

        return images.all()

    def create(self, request, *args, **kwargs):
        data = request.data.copy()
        data.update({'user': request.user})
        serializer = self.serializer_class(data=data)
        serializer.is_valid(raise_exception=True)

        image = serializer.save()

        return Response(serializer.data, status.HTTP_200_OK)

    @decorators.action(
        detail=True,
        methods=['PUT'],
        serializer_class=ImageFileSerializer,
        parser_classes=[parsers.MultiPartParser]
    )
    def upload(self, request, pk):
        obj = self.get_object()
        serializer = self.serializer_class(obj, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            publish_image_uploaded(str(obj.id), obj.title, obj.image.url)
            return Response(serializer.data)
        return Response(serializer.errors, status.HTTP_400_BAD_REQUEST)

    def partial_update(self, request, *args, **kwargs):
        kwargs['partial'] = True
        image = self.get_object()

        if request.user.username != image.user_id:
            return Response({}, status.HTTP_403_FORBIDDEN)

        return self.update(request, *args, **kwargs)

    @decorators.action(
	detail=True,
	methods=['PATCH'],
	serializer_class=ImageSerializer)
    def flag(self, request, pk):
        obj = self.get_object()
        data = { 'flagged': True }
        serializer = self.serializer_class(obj, data=data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status.HTTP_400_BAD_REQUEST)
