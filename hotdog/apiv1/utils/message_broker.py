#!/usr/bin/env python

from django.conf import settings
import json
import pika

def publish_image_uploaded(photo_id, title, image):
    connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
    channel = connection.channel()
    channel.queue_declare(queue=settings.IMAGE_UPLOADED_QUEUE, durable=True)

    body = json.dumps({
        "photo_id": photo_id,
        "title": title,
        "image": image
    })
    channel.basic_publish(exchange='',
                          routing_key=settings.IMAGE_UPLOADED_QUEUE,
                          body=body,
                          properties=pika.BasicProperties( delivery_mode=2))
    print(" [x] Sent photo_id {} for flag check".format(photo_id))

    connection.close()
