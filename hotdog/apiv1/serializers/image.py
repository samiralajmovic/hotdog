from rest_framework import serializers
from django.urls import include, path
from ...appv2.models import Image

class ImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Image
        fields = ['id', 'title', 'description', 'image', 'user', 'flagged' ]
        read_only_fields = ['id', 'image']

class ImageFileSerializer(serializers.ModelSerializer):
    class Meta:
        model = Image
        fields = ['image']
