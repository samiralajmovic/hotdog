import json
from rest_framework.authtoken.models import Token
from django.contrib.auth import authenticate
from django.contrib import auth
from django.contrib.auth.models import AnonymousUser, User
from rest_framework.test import APITestCase, APIClient
from django.test import TestCase, Client
from hotdog.appv2.models import Image

class UserTest(TestCase):

    def setUp(self):
        self.c = Client()

        self.user = User.objects.create_superuser(username="bot", email='bot@test.com', password='botbotbot')
        self.user.save()
        self.token = Token.objects.create(user=self.user)
        self.token.save()
        logged_in = self.c.login(username=self.user.username, password='test123')

    def test_landing_page(self):
        response = self.c.get('/')
        assert response.status_code == 200

    def test_create_image(self):
        response = self.c.post('/api/v1/images/', { 'title': 'lala land'}, HTTP_AUTHORIZATION='Token ' + self.token.key)
        assert response.status_code == 200

        queryset = Image.objects.all()
        assert len(queryset) == 1

    def test_get_images(self):
        Image.objects.create(title='Hello World', user=self.user)
        Image.objects.create(title='Hello World', user=self.user)

        response = self.c.get('/api/v1/images/', format='json')
        assert response.data['count'] == 2

    def test_get_image(self):
        image = Image.objects.create(title='Hello World', user=self.user)
        response = self.c.get('/api/v1/images/', {'id': image.id})

        assert response.data['results'][0]['id'] == str(image.id)

    def tearDown(self):
        pass

# 1. create image denied for unauthorized user 
# 2. upload image success for a user
# 3. change image title denied for unauthorized user
# 4. change image title success for user
# 5. Assert no other user can post on your behalf
# 6. logout
