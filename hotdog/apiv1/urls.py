from django.urls import include, path
from rest_framework import routers
from .views.user import UserViewSet
from .views.image import ImageViewSet

router = routers.DefaultRouter()
router.register(r'users', UserViewSet)
router.register(r'images', ImageViewSet)

app_name = 'apiv1'
urlpatterns = [
    path('', include(router.urls))
]
